import requests
from .keys import PEXELS_API_KEY


def get_pic(city, state):
    picture_search = "https://api.pexels.com/v1/search"
    params = {"query": f"{city} {state}", "per_page": 1}
    headers = {"Authorization": PEXELS_API_KEY}
    res = requests.get(picture_search, params=params, headers=headers)
    the_json = res.json()
    picture_url = the_json["photos"][0]["src"]["original"]
    return {"picture_url": picture_url}


def get_weather_data(city, state):
    pass
